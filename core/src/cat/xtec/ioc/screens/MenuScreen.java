package cat.xtec.ioc.screens;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import cat.xtec.ioc.SpaceRace;
import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.utils.Settings;

public class MenuScreen extends com.badlogic.gdx.scenes.scene2d.utils.ChangeListener implements Screen {

    protected Stage stage;
    private SpaceRace game;
    SpaceRace spaceRace;


    public MenuScreen(SpaceRace game)
    {
        this.game=game;
        this.spaceRace=spaceRace;

        // Creem la càmera de les dimensions del joc
        OrthographicCamera camera = new OrthographicCamera(Settings.GAME_WIDTH, Settings.GAME_HEIGHT);
        // Posant el paràmetre a true configurem la càmera per a
        // que faci servir el sistema de coordenades Y-Down
        camera.setToOrtho(true);

        // Creem el viewport amb les mateixes dimensions que la càmera
        StretchViewport viewport = new StretchViewport(Settings.GAME_WIDTH, Settings.GAME_HEIGHT, camera);

        // Creem l'stage i assginem el viewport
        stage = new Stage(viewport);

        // Afegim el fons
        stage.addActor(new Image(AssetManager.background));


        Gdx.input.setInputProcessor(stage);
       anadirTextButtons();
    }

    private void anadirTextButtons() {
        TextButton.TextButtonStyle estilo = new TextButton.TextButtonStyle();
        estilo.font = AssetManager.font;

        TextButton facil = new TextButton("Facil", estilo);
        facil.addListener(this);
        facil.setName("1");



        facil.setPosition(Settings.GAME_WIDTH / 2 - facil.getWidth() / 2,
                Settings.GAME_HEIGHT * 0.25f - facil.getHeight() / 2);



        TextButton medio = new TextButton("Medio", estilo);
        medio.setName("2");
        medio.addListener(this);
        medio.setPosition(Settings.GAME_WIDTH / 2 - medio.getWidth() / 2,
                Settings.GAME_HEIGHT * 0.5f - medio.getHeight() / 2);

        TextButton dificil = new TextButton("Dificil", estilo);
        dificil.setName("3");
        dificil.addListener(this);
        dificil.setPosition(Settings.GAME_WIDTH / 2 - dificil.getWidth() / 2,
                Settings.GAME_HEIGHT * 0.75f - dificil.getHeight() / 2);

        Label infoLabel = new Label("Selecciona la dificultad", new Label.LabelStyle(AssetManager.font, null));

        infoLabel.setFontScale(0.25f);
        infoLabel.setPosition(Settings.GAME_WIDTH / 2 - infoLabel.getWidth() * 0.30f, infoLabel.getHeight() / 2);

        stage.addActor(facil);
        stage.addActor(medio);
        stage.addActor(dificil);
        stage.addActor(infoLabel);
    }


    @Override
    public void show() {

    }



    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render(float delta) {
        stage.draw();
        stage.act(delta);

    }
    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
    }


    @Override
    public void changed(ChangeEvent event, Actor actor) {

//pasarle la dificultad segun apretemos
        int dificultad=0;
        if(actor.getName().equals("1")){
            dificultad=1;
        }else if(actor.getName().equals("2")){
            dificultad=2;
        }else if(actor.getName().equals("3")){
            dificultad=3;
        }

        if(actor!=null) {
            Gdx.app.log("Menu","entra");
            AssetManager.dificultad=dificultad;
            game.setScreen(new GameScreen(stage.getBatch(), stage.getViewport()));
            dispose();
        }
        }
    }

